<?php
	require_once '../DB/db.php';
	require_once '../global_var.php';

/*-------------------------------------------------------------------------------------------------------------------
										CREATE_HOTEL
---------------------------------------------------------------------------------------------------------------------*/			
	// function create_hotel($idHotel, $nomHotel, $adrHotel, $telHotel) {
	// 	$connection = createConnection();

	// 	$create_hotel_query = " INSERT INTO HOTEL (IdHotel, NomHotel, AdresseHotel, TelHotel) 
	// 							VALUES ($idHotel, '$nomHotel', '$adrHotel', '$telHotel')";

	// 	$query_result = $connection->query($create_hotel_query);

	// 	if( ! $query_result) {
	// 		echo "create_hotel() :  " . $connection->error ."<br>";
	// 		$connection->close();
	// 		return;
	// 	}
	// 	$connection->close();
		
	// } // create_hotel()


	function create_hotel($hotel) {
		$connection = createConnection();

		$idHotel  = $hotel->getHotelId();
		$nomHotel = $hotel->getHotelName();
		$adrHotel = $hotel->getHotelAddress();
		$telHotel = $hotel->getHotelPhoneNumber();


		$create_hotel_query = " INSERT INTO HOTEL (IdHotel, NomHotel, AdresseHotel, TelHotel) 
								VALUES ($idHotel, '$nomHotel', '$adrHotel', '$telHotel')";

		$query_result = $connection->query($create_hotel_query);

		if( ! $query_result) {
			echo "create_hotel() :  " . $connection->error ."<br>";
			$connection->close();
			return;
		}
		$connection->close();
		
	} // create_hotel()




/**************************************************************************************************************
**************************************************************************************************************/



 /*-------------------------------------------------------------------------------------------------------------------
										CREATE_ROOM
---------------------------------------------------------------------------------------------------------------------*/	
	// function create_room($idChambre, $idHotel, $libelleType) {
	// 	$connection = createConnection();
	// 	$insert_room_query = "INSERT INTO CHAMBRE (IdChambre, IdHotel, LibelleType)
	// 						  VALUES ($idChambre, $idHotel, '$libelleType')";

	// 	$query_result = $connection->query($insert_room_query);

	// 	if( ! $query_result) {
	// 		echo " Erreur : [ create_room() ]:  " . $connection->error ."<br>";
	// 		$connection->close();
	// 		return;
	// 	}


	// } // create_room()


	function create_room($room) {
		$connection = createConnection();

		$idChambre 	 = $room->getRoomId();
		$idHotel 	 = $room->getHotelId();
		$libelleType = $room->getTypeWording();

		$insert_room_query = " INSERT INTO CHAMBRE (IdChambre, IdHotel, LibelleType) 
							   VALUES ($idChambre, '$idHotel', '$libelleType')";

		$query_result = $connection->query($insert_room_query);

		if( ! $query_result) {
			echo " Erreur : [ create_room() ]:  " . $connection->error ."<br>";
			$connection->close();
			return;
		}

		$connection->close();


	} // create_room()

/**************************************************************************************************************
**************************************************************************************************************/



 /*-------------------------------------------------------------------------------------------------------------------
										CREATE_TYPE
---------------------------------------------------------------------------------------------------------------------*/	
	// function create_type($libelleType, $nbLitsSimples, $nbLitsDoubles, $possedeTel, $possedeTV, $possedeHD, $prixType) {
	// 	$connection = createConnection();

	// 	$insert_type_query = "INSERT INTO TYPE VALUES ('$libelleType', $nbLitsSimples, $nbLitsDoubles, $possedeTel, $possedeTV, $possedeHD, $prixType)";

	// 	$query_result = $connection->query($insert_type_query);

	// 	if( ! $query_result) {
	// 		echo " Erreur : [ create_type() ]:  " . $connection->error ."<br>";
	// 		$connection->close();
	// 		return;
	// 	}
		// $connection->close();

	// } // create_type()

	function create_type($type) {
		$connection = createConnection();

		$libelleType   = $type->getTypeWording();
		$nbLitsSimples = $type->getNbOfSingleBeds();
		$nbLitsDoubles = $type->getNbOfDoubleBeds();
		$possedeTel    = $type->getHasPhone();
		$possedeTV 	   = $type->getHasTV();
		$possedeHD 	   = $type->getHasHD();
		$prixType 	   = $type->getTypePrice();



		$insert_type_query = "INSERT INTO TYPE VALUES ('$libelleType', $nbLitsSimples, $nbLitsDoubles, $possedeTel, $possedeTV, $possedeHD, $prixType)";

		$query_result = $connection->query($insert_type_query);

		if( ! $query_result) {
			echo " Erreur : [ create_type() ]:  " . $connection->error ."<br>";
			$connection->close();
			return;
		}

		$connection->close();

	} // create_type()


/**************************************************************************************************************
**************************************************************************************************************/

/*-------------------------------------------------------------------------------------------------------------------
										CREATE_RESERVATION
---------------------------------------------------------------------------------------------------------------------*/
	// function create_reservation($reservationId, $dateOfReservation,
	// 							$dateOfBeginningOfStay, $dateOfEndOfStay,
	// 							$reservationState, $billId,
	// 							$dateOfPayment, $totalAmount,
	// 							$depositePaid, $customerId,
	// 							$roomId, $hotelId)
	// {
	// 	$roomExists = check_if_room_exists($roomId, $hotelId);
	// 	if(! $roomExists) { echo "ERREUR LA CHAMBRE N'EXISTE PAS! <br>"; return; }

	// 	$reservationDone = insert_tuple_reservation($reservationId, $dateOfReservation,
	// 												$dateOfBeginningOfStay, $dateOfEndOfStay,
	// 												$reservationState, $billId,
	// 												$dateOfPayment, $totalAmount,
	// 												$depositePaid, $customerId);
	// 	if( !$reservationDone) { echo "Réservation non effectuée<br>"; return; }

	// 	insert_tuple_affecte($roomId, $hotelId, $reservationId);

	// }


	function create_reservation($reservation, $roomId, $hotelId)
	{
		$reservationId = $reservation->getIdReservation();
		$dateOfReservation = $reservation->getDateOfReservation();
		$dateOfBeginningOfStay = $reservation->getDateOfBeginningOfStay();
		$dateOfEndOfStay = $reservation->getDateOfEndOfStay();
		$reservationState = $reservation->getReservationState();
		$billId = $reservation->getBillId();
		$dateOfPayment = $reservation->getDateOfPayment();
		$totalAmount = $reservation->getTotalAmount();
		$depositePaid = $reservation->getDepositePaid();
		$customerId = $reservation->getCustomerId();





		$roomExists = check_if_room_exists($roomId, $hotelId);
		if(! $roomExists) { echo "ERREUR LA CHAMBRE N'EXISTE PAS! <br>"; return; }

		$reservationDone = insert_tuple_reservation($reservationId, $dateOfReservation,
													$dateOfBeginningOfStay, $dateOfEndOfStay,
													$reservationState, $billId,
													$dateOfPayment, $totalAmount,
													$depositePaid, $customerId);
		if( !$reservationDone) { echo "Réservation non effectuée<br>"; return; }

		insert_tuple_affecte($roomId, $hotelId, $reservationId);

	}

//----------------------------------------------------------------------------------------------------

	function check_if_room_exists($roomId, $hotelId) {
		$connection = createConnection();
		$room_hotel_exists = "SELECT COUNT(*) AS NB_MATCHED_ROOMS
							  FROM CHAMBRE
							  WHERE IdHotel = $hotelId
								AND IdChambre = $roomId";

		$query_result = $connection->query($room_hotel_exists);
		if( ! $query_result) {
			echo " Erreur : [ create_reservation() ]:  " . $connection->error ."<br>";
			$connection->close();
			return;
		}
		$results = $query_result->fetch_assoc();
		$nb_rooms_found = $results["NB_MATCHED_ROOMS"];

		// print_r($nb_rooms_found);
		// echo "<br>";

		// print_r($nb_rooms_found);
		if($nb_rooms_found != 1) {
			$connection->close();
			return FALSE;
		}

		echo "la chambre existe bel et bien ! => Réservation peut être effectuée<br>";
		$connection->close();
		return TRUE;

	} // check_if_room_exists()

//----------------------------------------------------------------------------------------------------

	function insert_tuple_reservation($reservationId, $dateOfReservation,
									  $dateOfBeginningOfStay, $dateOfEndOfStay,
									  $reservationState, $billId,
									  $dateOfPayment, $totalAmount,
									  $depositePaid, $customerId)
	{
		$connection = createConnection();

		$insert_reservation_query = "INSERT INTO RESERVATION VALUES($reservationId, '$dateOfReservation',
																	'$dateOfBeginningOfStay', '$dateOfEndOfStay',
																	'$reservationState', $billId,
																	'$dateOfPayment', $totalAmount,
																	'$depositePaid', $customerId)";

		$query_result = $connection->query($insert_reservation_query);
		if( ! $query_result) {
			echo " Erreur : [ create_reservation() ]:  " . $connection->error ."<br>";
			$connection->close();
			return FALSE;
		}

		echo "INSERTED! <br>";
		$connection->close();

		return TRUE;

	}
//----------------------------------------------------------------------------------------------------
	function insert_tuple_affecte($roomId, $hotelId, $reservationId) {
		$connection = createConnection();

		$insert_affecte_query = "INSERT INTO AFFECTE VALUES($roomId, $hotelId, $reservationId)";
		$query_result = $connection->query($insert_affecte_query);

		if ( ! $query_result) {
			echo " Erreur : [ create_reservation() ]:  " . $connection->error ."<br>";
			$connection->close();
			return;
		}

		echo "INSERT INTO AFFECTE WORKED! <br>";
		$connection->close();
	}
?>