<?php
	require_once '../DB/db.php';
	require_once '../global_var.php';
/*-------------------------------------------------------------------------------------------------------------------
										DISPLAY_HOTEL
---------------------------------------------------------------------------------------------------------------------*/
	function display_hotels() {
		$connection = createConnection(); // db.php

		$display_hotels_query = "SELECT *
								 FROM HOTEL";

		$query_result = $connection->query($display_hotels_query);

		if( ! $query_result) {
			echo " Erreur : [ display_hotels() ]:  " . $connection->error ."<br>";
			$connection->close();
			return;
		}

		if($query_result->num_rows > 0) {
			while( $hotel = $query_result->fetch_assoc() ) {
				echo "IdHotel : " . 	 $hotel["IdHotel"] . "<br>" .
					 "NomHotel : " . 	 $hotel["NomHotel"] . "<br>" .
					 "AdresseHotel : " . $hotel["AdresseHotel"] . "<br>" .
					 "TelHotel : " . 	 $hotel["TelHotel"]. "<br><br>";
			}
		} else {
			echo "0 result...<br>";
		}

		echo "------------------------------------------------<br>";

		$connection->close();

	}

/**************************************************************************************************************
**************************************************************************************************************/

/*-------------------------------------------------------------------------------------------------------------------
										DISPLAY_ROOMS
---------------------------------------------------------------------------------------------------------------------*/
	function display_rooms() {
		$connection = createConnection(); // db.php

		$display_rooms_query = "SELECT *
								FROM CHAMBRE";

		$query_result = $connection->query($display_rooms_query);

		if( ! $query_result) {
			echo " Erreur : [ display_rooms() ]:  " . $connection->error ."<br>";
			$connection->close();
			return;
		}

		if($query_result->num_rows > 0) {
			while( $room = $query_result->fetch_assoc() ) {
				echo "IdChambre : " . 	 $room["IdChambre"] . "<br>" .
					 "IdHotel : " . 	 $room["IdHotel"] . "<br>" .
					 "LibelleType : " .  $room["LibelleType"] . "<br><br>";
			}
		} else {
			echo "0 result...<br>";
		}

		echo "------------------------------------------------<br>";

		$connection->close();
	}

/**************************************************************************************************************
**************************************************************************************************************/
/*-------------------------------------------------------------------------------------------------------------------
										DISPLAY_TYPES
---------------------------------------------------------------------------------------------------------------------*/
	function display_types() {
		$connection = createConnection(); // db.php

		$display_types_query = "SELECT *
								FROM TYPE";

		$query_result = $connection->query($display_types_query);

		if( ! $query_result) {
			echo " Erreur : [ display_types() ]:  " . $connection->error ."<br>";
			$connection->close();
			return;
		}

		if($query_result->num_rows > 0) {
			while( $type = $query_result->fetch_assoc() ) {
				if($type["PossedeTel"] == 1) {
					$type["PossedeTel"] = "TRUE";
				} elseif($type["PossedeTel"] == 0) {
					$type["PossedeTel"] = "FALSE";
				} else {
					$type["PossedeTel"] = "NULL";
				}

				if($type["PossedeTV"] == 1) {
					$type["PossedeTV"] = "TRUE";
				} elseif($type["PossedeTV"] == 0) {
					$type["PossedeTV"] = "FALSE";
				} else {
					$type["PossedeTV"] = "NULL";
				}


				if($type["PossedeHD"] == 1) {
					$type["PossedeHD"] = "TRUE";
				} elseif($type["PossedeHD"] == 0) {
					$type["PossedeHD"] = "FALSE";
				} else {
					$type["PossedeHD"] = "NULL";
				}


				if( ! $type["PrixType"]) {
					$type["PrixType"] = 0;
				}


				echo "Libellé du type : " . $type["LibelleType"] . "<br>" .
					 "Nombre de lits simples : " . 	 $type["NbLitsSimples"] . "<br>" .
					 "Nombre de lits doubles : " . 	 $type["NbLitsDoubles"] . "<br>" .
					 "Possede un téléphone : " . 	 $type["PossedeTel"] . "<br>" .
					 "Possède une télévision : " . 	 $type["PossedeTV"] . "<br>" .
					 "Possède chaîne HD : " . 	 	 $type["PossedeHD"] . "<br>" .
					 "Prix du type de chambre : " .  $type["PrixType"] . "€<br><br>";
			}
		} else {
			echo "0 result...<br>";
		}

		echo "------------------------------------------------<br>";

		$connection->close();
	}

/**************************************************************************************************************
**************************************************************************************************************/
/*-------------------------------------------------------------------------------------------------------------------
										DISPLAY_RESERVATIONS (AFFECTE)
---------------------------------------------------------------------------------------------------------------------*/

	function display_reservations() {
		$connection = createConnection(); // db.php

		$get_reservations_query = " SELECT *
									FROM AFFECTE
									ORDER BY IdReservation ASC";

		$query_result = $connection->query($get_reservations_query);

		if($query_result->num_rows > 0) {
			while( $reservation = $query_result->fetch_assoc() ) {
				echo "IdChambre : " . $reservation["IdChambre"] . "<br>" .
					 "IdHotel : " . $reservation["IdHotel"] . "<br>" .
					 "IdReservation : " . $reservation["IdReservation"] . "<br><br>";
			}
		} else {
			echo "0 result...<br>";
		}

		$connection->close();

	}
?>