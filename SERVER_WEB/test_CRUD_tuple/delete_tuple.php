<?php
	require_once '../DB/db.php';
	require_once '../global_var.php';
/*-------------------------------------------------------------------------------------------------------------------
										DELETE_HOTEL
---------------------------------------------------------------------------------------------------------------------*/
	function delete_hotel($idHotel) {
		$connection = createConnection(); // db.php

		$rooms_deletion_query = "DELETE FROM CHAMBRE
								 WHERE IdHotel = $idHotel";


		$hotel_deletion_query = "DELETE FROM HOTEL
								 WHERE IdHotel = $idHotel";


		try {
			$query_result = $connection->query($rooms_deletion_query);
			$query_result = $connection->query($hotel_deletion_query);

		} catch (mysqli_sql_exception $e) {
			throw $e;
		}

		$connection->close();
	}



/**************************************************************************************************************
**************************************************************************************************************/

/*-------------------------------------------------------------------------------------------------------------------
										DELETE_ROOM
---------------------------------------------------------------------------------------------------------------------*/
	function delete_room($roomId, $hotelId) {
		$connection = createConnection(); // db.php
		$room_deletion_query = "DELETE FROM CHAMBRE
		 						WHERE IdHotel = $hotelId
		 							AND IdChambre = $roomId";

		try{
			$query_result = $connection->query($room_deletion_query);

		} catch(mysqli_sql_exception $e) {
			throw $e;

		}

		echo "Deleted! <br>";

		$connection->close();
	}


/**************************************************************************************************************
**************************************************************************************************************/

/*-------------------------------------------------------------------------------------------------------------------
										DELETE_TYPE
---------------------------------------------------------------------------------------------------------------------*/
	function delete_type($typeWording) {
		$connection = createConnection(); // db.php

		// TODO : choose what to do : update rooms with the type that will be deleted -> before deleting type, delete all rooms with the type to delete

		$type_deletion_query = "DELETE FROM TYPE
								WHERE LibelleType = '$typeWording'";

		try{
			$query_result = $connection->query($type_deletion_query);

		} catch(mysqli_sql_exception $e) {
			throw $e;

		}

		echo "Deleted! <br>";

		$connection->close();
	}


/**************************************************************************************************************
**************************************************************************************************************/

/*-------------------------------------------------------------------------------------------------------------------
										DELETE_RESERVATION
---------------------------------------------------------------------------------------------------------------------*/
	function delete_reservation($reservationId) {
		$connection = createConnection(); // db.php


	/* get roomId and hotelId */
		$get_rooms_reservation = "SELECT IdChambre, IdHotel
								  FROM AFFECTE
								  WHERE IdReservation = $reservationId";


		try{
			$query_result = $connection->query($get_rooms_reservation);
			if($query_result->num_rows > 0) {
				$reservation = $query_result->fetch_assoc();

				$roomId = $reservation["IdChambre"];
				$hotelId = $reservation["IdHotel"];

			} else {
				echo "Delete : 0 result...<br><br>";
				return;
			}

		} catch(mysqli_sql_exception $e) {
			throw $e;

		}	

	/* delete tuple in affecte */
		$affecte_deletion_query = "DELETE FROM AFFECTE
								   WHERE IdChambre = $roomId
								   	AND IdHotel = $hotelId
								   	AND IdReservation = $reservationId";

		try{
			$query_result = $connection->query($affecte_deletion_query);

		} catch(mysqli_sql_exception $e) {
			throw $e;

		}

	/* delete tuple in reservation */
		$reservation_deletion_query = " DELETE FROM RESERVATION
										WHERE IdReservation = $reservationId";

		try{
			$query_result = $connection->query($reservation_deletion_query);

		} catch(mysqli_sql_exception $e) {
			throw $e;

		}

		echo "Deleted! <br>";
		$connection->close();
	}

?>