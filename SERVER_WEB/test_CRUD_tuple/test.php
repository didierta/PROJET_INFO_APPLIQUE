<?php
	require_once '../DB/db.php';
	require_once '../global_var.php';

	require_once 'create_tuple.php';
	require_once 'modify_tuple.php';
	require_once 'delete_tuple.php';
	require_once 'display_table.php';

	require_once '../class/Category.php';
	require_once '../class/Customer.php';
	require_once '../class/Hotel.php';
	require_once '../class/Reservation.php';
	require_once '../class/Room.php';
	require_once '../class/Service.php';
	require_once '../class/Type.php';


function runTest() {
	// test_hotel_actions();
	// test_room_actions();
	// test_type_actions();
	// TODO : create reservation with object
	// delete_reservation(9999);
	test_reservation_actions();







} // runTest()

/**************************************************************************************************************
**************************************************************************************************************/


function test_hotel_actions() {
/* TEST HOTEL ACTIONS */
	// create_hotel(999, 'MYHOTEL', '200, impasse des Cerises 13001 Marseille', '0293127');
	// display_hotels();
	// delete_hotel(999);
	// display_hotels();

	/*  OBJ */
	$hotel = new Hotel(999, 'MYHOTEL', '200, impasse des Cerises 13001 Marseille', '0293127');
	create_hotel($hotel);
	display_hotels();
	delete_hotel(999);
	display_hotels();
}

/**************************************************************************************************************
**************************************************************************************************************/

function test_room_actions() {
/* TEST ROOM ACTIONS */
	// create_room(10, 1, 'GOULAG');
	// display_rooms();
	// delete_room(10, 1);
	// display_rooms();

	/*  OBJ */
	$room = new Room(10, 1, 'GOULAG');
	create_room($room);
	display_rooms();
	delete_room(10, 1);
	display_rooms();	
}

/**************************************************************************************************************
**************************************************************************************************************/
function test_type_actions() {
/* TEST TYPE ACTIONS */
	// create_type('MYTYPE', 99, 99, TRUE, TRUE, TRUE, 999);
	// display_types();
	// delete_type('MYTYPE');
	// display_types();

	/*  OBJ */
	$type = new Type('MYTYPE', 99, 99, TRUE, TRUE, TRUE, 999);
	create_type($type);
	display_types();
	delete_type('MYTYPE');
	display_types();

}

/**************************************************************************************************************
**************************************************************************************************************/

function test_reservation_actions() {
/* TEST RESERVATION ACTIONS */
	// room and hotel exists
	// create_reservation(99999, '2015-04-16',
	// 				   '2015-05-01', '2015-05-16',
	// 				   'EN ATTENTE DE CONFIRMATION', 1,
	// 				   99999, 'NULL',
	// 				   'FALSE', 1,
	// 				   1, 1);
	// echo "**************************************************<br>";
	// display_reservations();
	// echo "**************************************************<br>";
	// delete_reservation(99999);
	// echo "**************************************************<br>";
	// display_reservations();

	// room and hotel does not exists
	// create_reservation(1, '2015-04-16',
	// 				   '2015-05-01', '2015-05-16',
	// 				   'EN ATTENTE DE CONFIRMATION', 1,
	// 				   NULL, 0,
	// 				   'FALSE', 1,
	// 				   99, 99);

/* OBJ */
	$reservation = new Reservation(9999, '2015-04-16',
					   			   '2015-05-01', '2015-05-16',
					   			   11, NULL,
					   			   1);

	create_reservation($reservation, 1, 1);
	echo "**************************************************<br>";
	display_reservations();
	echo "**************************************************<br>";
	delete_reservation(9999);
	echo "**************************************************<br>";
	display_reservations();

} // test_reservation_actions()


?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Reservation de chambres</title>
    </head>

    <body>
        <div>
        <?php
			runTest();
        ?>

        </div>



        <!--
        <script type="text/javascript" src="./bootstrap/css/bootstrap.css"></script>

        -->
        <script type="text/javascript" src="./js/reservation.js"></script>
    </body>
</html>