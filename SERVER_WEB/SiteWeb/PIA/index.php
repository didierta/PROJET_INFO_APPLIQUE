<!DOCTYPE html>
<!--
	antoine.thebaud.etu.perso.luminy.univ-amu.fr/PIA/index.php
	http://localhost:8080/WEB/PIA/SiteWeb/index.html
-->

<html lang="fr">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8"> 
		<title>Chaine d'hotel</title>
		<!--<meta name="generator" content="Bootply" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">-->
		<link rel="stylesheet" href="bootstrap/css/bootstrap.css" media="screen">
		<link rel="stylesheet" href="bootstrap/css/bootswatch.min.css">
		<link rel="stylesheet" href="bootstrap/css/docs.min.css">
		<link rel="stylesheet" type="text/css" href="http://twitter.github.com/bootstrap/assets/css/bootstrap.css">
	        
		<style type="text/css">
			.container {
				max-width: 65%;
			}
		</style>
	</head>
    
	<body id="content">

		<!--menu (ruban)-->

		<?php include("contents/navbar.php"); ?>

		<!--contenu principal-->
		
		<div class="container">
			<div class="jumbotron text-center">
				<h1>Lorem ipsum</h1>
				<p class="lead">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
			</div>
			
			<div>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer venenatis ullamcorper ligula, nec pulvinar felis 
					vulputate non. Nam non massa nibh. Etiam eleifend ex id dui mollis, porta eleifend orci pharetra. In id fermentum felis. 
					Fusce lacinia maximus ullamcorper. Integer vitae nisi nibh. Fusce eget vulputate tortor, eget efficitur ligula. Nunc 
					accumsan suscipit magna quis luctus. Sed id sem at felis consectetur congue. Cras a erat porta, malesuada est at, molestie
					 sapien. Morbi iaculis at nunc sed feugiat. Morbi vitae imperdiet magna, at tempor nisl..</p>
			</div>
			
			<div class="row">
				<div class="col-lg-6">
					<h3>Integer vitae nisi nibh <a class="btn btn-success btn-mini" id="FIFO"></i>show</a></h3>
					<div id="contenu" style="display:none;">
						<p><b>-</b> Fusce eget vulputate tortor, eget efficitur ligula. Nunc accumsan suscipit magna quis luctus.
						Sed id sem at felis consectetur congue.</p>
						<p><b>-</b> Etiam eleifend ex id dui mollis, porta eleifend orci pharetra.</p>
						<p><b>-</b> Morbi iaculis at nunc sed feugiat. Morbi vitae imperdiet magna, at tempor nisl..</p>
					</div>
					
					<h3>Fusce lacinia maximo  <a class="btn btn-success btn-mini" id="FIFO2"></i>show</a></h3>
					<div id="contenu2" style="display:none;">
						<p><b>-</b> Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem
						aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam
						voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem
						sequi nesciunt</p>
						<p><b>-</b> At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque
						corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui
						officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio</p>
					</div>
				</div>
				
				<div class="col-lg-6">
					<h3>Morbi vitae magna <a class="btn btn-success btn-mini" id="FIFO3"></i>show</a></h3>
					<div id="contenu3" style="display:none;">
						<p><b>- Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat.</p>
						<p><b>- Et harum quidem rerum facilis est et expedita distinctio</p>
						<p><b>- Morbi iaculis at nunc sed feugiat. Morbi vitae imperdiet magna, at tempora temporum</p><br/>
					</div>
				</div>
			
			</div>
		  
			<hr>
		  
			<div class="jumbotron text-center">
				<p class="lead">Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae
				sint et molestiae non recusandae</p>
				<p><a class="btn btn-large btn-info" href="http://www.univ-amu.fr/" target="ext">UNIV-AMU</a></p>
			</div>
		  
		</div>

		
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
		<script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>

		<script src="script.js"></script>
	</body>
</html>