//=====================================BOUTONS=========================================\\
	
$(document).ready(function(){
	
	//Deconnexion
	$("#disconnect").click(function () {
		$("#connected").hide();
		$("#disconnect").hide();
		$(form1).fadeIn();
		$(form1).trigger('reset');
	});
	
	//SHOW/HIDE Informations générales
	$("#FIFO").click(function () {
		var obj = document.getElementById("contenu");
		if ($(obj).is(":hidden")) {
			$(obj).fadeIn();
			this.text="hide";
		} else {
			$(obj).fadeOut();
			this.text="show";
		}
	});
	
	//SHOW/HIDE Date et durée du stage
	$("#FIFO2").click(function () {
		var obj = document.getElementById("contenu2");
		if ($(obj).is(":hidden")) {
			 $(obj).fadeIn();
			 this.text="hide";
		} else {
			$(obj).fadeOut();
			this.text="show";
		}
	});
	
	//SHOW/HIDE Objectifs des stages
	$("#FIFO3").click(function () {
		var obj = document.getElementById("contenu3");
		if ($(obj).is(":hidden")) {
			$(obj).fadeIn();
			this.text="hide";
		} else {
			$(obj).fadeOut();
			this.text="show";
		}
	});

});