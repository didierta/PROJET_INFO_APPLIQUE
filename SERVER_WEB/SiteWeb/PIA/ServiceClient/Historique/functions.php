<?php

    require("../../db.php");

    function getIdentity($idClient) {

        $connection = createConnection();

        $req = "SELECT Nom, Prenom
                FROM CLIENT
                WHERE IdClient = ".$idClient;

        $queryClient = $connection->query($req);
        $rowClient = $queryClient->fetch_assoc();

        $nbPassages = 0;
        $req = "SELECT *
                FROM ACCOMPAGNE
                WHERE IdClient = ".$idClient;

        $queryPassages = $connection->query($req);
        $nbPassages = $queryPassages->num_rows;

        $result = array("nomClient" => $rowClient["Nom"],
                        "prenomClient"=> $rowClient["Prenom"],
                        "nbPassages" => $nbPassages);

        return $result;
    }

    function getHistory($idClient) {

        $connection = createConnection();

        $req = "SELECT IdReservation, DateDebSejour, DateFinSejour, MontantTotal
                FROM RESERVATION
                WHERE IdReservation IN (
                        SELECT IdReservation
                        FROM ACCOMPAGNE
                        WHERE IdClient = ".$idClient.")";

        $queryRes = $connection->query($req);

        while($rowRes = $queryRes->fetch_assoc()) {

            $tabRes[] = array("idRes"   => $rowRes["IdReservation"],
                              "dateDeb" => $rowRes["DateDebSejour"],
                              "dateFin" => $rowRes["DateFinSejour"],
                              "montant" => $rowRes["MontantTotal"] );

        }
        return $tabRes;
    }

    /********************************************************
                  Génère les lignes du tableau
    ********************************************************/

    if ($_GET["mode"] == "title") {

        $response = getIdentity($_GET["id_client"]);
        if ($response["nbPassages"] != 0) {
            echo $response["prenomClient"]." ".$response["nomClient"].
             " : ".$response["nbPassages"]." ".(intval($response["nbPassages"]) > 1 ? "passages" : "passage");
        } else {
            echo "Ce client n'existe pas";
        }

    } else if ($_GET["mode"] == "rows") {
        
        $response = getHistory($_GET["id_client"]);
        //echo "<pre>".print_r($response, true)."</pre>";

        if (count($response) != 0) {

            foreach($response as $reservation) {
                echo '<tr>';
                echo '<td>'.$reservation["idRes"].'</td>';
                echo '<td>'.$reservation["dateDeb"].'</td>';
                echo '<td>'.$reservation["dateFin"].'</td>';
                echo '<td>'.$reservation["montant"].'</td>'; 
                echo '</tr>';
            }
        }
    }

?>