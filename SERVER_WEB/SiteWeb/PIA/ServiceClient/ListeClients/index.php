<!DOCTYPE html>

<html lang="fr">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8"> 
        <title>Clientèle actuelle</title>

        <link rel="stylesheet" href="../../bootstrap/css/bootstrap.css" media="screen">
        <link rel="stylesheet" href="../../bootstrap/css/bootswatch.min.css">
        <link rel="stylesheet" href="../../bootstrap/css/docs.min.css">
        <link rel="stylesheet" type="text/css" href="http://twitter.github.com/bootstrap/assets/css/bootstrap.css">
            
        <style type="text/css">
            .container {
                max-width: 65%;
            }
        </style>
    </head>
    
    <body id="content">

        <!--menu (ruban)-->

        <?php include("../../contents/navbar.php"); ?>

        <!--formulaire de requête-->

        <div class="container">
            <h1><u>Clientèle actuelle</u></h1>
            <h3>Choisissez un hôtel</h3>
            <form class="form-inline" role="form">
                <div class="form-group">
                    <label>Identifiant de l'hôtel :</label>
                    <input type="text" name="id_hotel" id="id_hotel" class="form-control" onblur="verifId(this)"><br>
                </div>
                <input type="button" class="btn btn-default" onclick="getData()" value="Valider"/>
            </form>
        </div>
        <br><hr>

        <!--affichage du résultat de la requête-->

        <div class="container">
            <h3 id="resultTitle">Résultat de la demande</h3>
            <table class="table" id="table">
            <thead>
              <tr>
                <th>Numéro de client</th>
                <th>Nom</th>
                <th>Prénom</th>
                <th>Identifiant de la réservation</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
            </table>
        </div>
        
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>

        <script src="script.js"></script>
    </body>
</html>
