<?php

	require("../../db.php");

    function getClientList($idHotel) {

        $connection = createConnection();

        $nbClients = 0;

        $currentdate = date("Y-m-d");

        $req = "SELECT CLIENT.IdClient, Nom, Prenom, IdReservation
                FROM CLIENT, ACCOMPAGNE
                WHERE CLIENT.IdClient = ACCOMPAGNE.IdCLient AND IdReservation IN (
                        SELECT IdReservation
                        FROM AFFECTE
                        WHERE IdHotel = ".$idHotel.") AND IdReservation IN (
                                SELECT IdReservation
                                FROM RESERVATION
                                WHERE \"".$currentdate."\" >= DateDebSejour AND \"".$currentdate."\" <= DateFinSejour)";

        $queryClients = $connection->query($req);

        //nombre total de client
        $nbClients = $queryClients->num_rows;
        //pour chaque client :
        while($rowClient = $queryClients->fetch_assoc()) {

            $tabClients[] = array("idClient"=> $rowClient["IdClient"],
                                  "nom"     => $rowClient["Nom"],
                                  "prenom"  => $rowClient["Prenom"],
                                  "idRes"   => $rowClient["IdReservation"] );

        }

        $result = array("nbClients" => $nbClients,
                        "tabClients"=> $tabClients);

        return $result;
    }

    /********************************************************
				  Génère les lignes du tableau
	********************************************************/

	$response = getClientList($_GET["id_hotel"]);
    //echo "<pre>".print_r($response, true)."</pre>";

    if ($_GET["mode"] == "title") {

        echo "Nombre de clients : ".$response["nbClients"];

    } else if ($_GET["mode"] == "rows") { 

        if ($response["nbClients"] != 0) {

            foreach($response["tabClients"] as $client) {
                echo '<tr>';
                echo '<td>'.$client["idClient"].'</td>';
                echo '<td>'.$client["nom"].'</td>'; 
                echo '<td>'.$client["prenom"].'</td>';
                echo '<td>'.$client["idRes"].'</td>'; 
                echo '</tr>';
            }
        }
    }

?>