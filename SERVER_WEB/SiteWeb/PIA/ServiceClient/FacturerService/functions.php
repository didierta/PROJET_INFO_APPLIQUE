<?php

    require("../../db.php");

    function getServiceList() {

        $connection = createConnection();

        $req = "SELECT LibelleService
                FROM   SERVICE";

        $queryService = $connection->query($req);

        while($rowService = $queryService->fetch_assoc()) {
            $tabServices[] = $rowService["LibelleService"];
        }
        return $tabServices;
    }

    /********************************************************
                  Génère les lignes du tableau
    ********************************************************/

    //$response = getClientList($_GET["id_hotel"]);
    //echo "<pre>".print_r($response, true)."</pre>";

    if ($_GET["request"] == "services") {

        $listeServices = getServiceList();

        if (count($listeServices) != 0) {

            foreach($listeServices as $service) {
                echo "<option>".$service."</option>";
            }
        }
    } else if ($_GET["mode"] == "rows") { 

        if ($response["nbClients"] != 0) {

            foreach($response["tabClients"] as $client) {
                echo '<tr>';
                echo '<td>'.$client["idClient"].'</td>';
                echo '<td>'.$client["nom"].'</td>'; 
                echo '<td>'.$client["prenom"].'</td>';
                echo '<td>'.$client["idRes"].'</td>'; 
                echo '</tr>';
            }
        }
    }

?>