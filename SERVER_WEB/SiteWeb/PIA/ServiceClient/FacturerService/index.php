<!DOCTYPE html>

<html lang="fr">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8"> 
        <title>Facturer un service</title>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
            
        <style type="text/css">
            .container {
                max-width: 65%;
            }
        </style>
    </head>
    
    <body id="content">

        <!--menu (ruban)-->

        <?php include("../../contents/navbar.php"); ?>

        <!--formulaire de saisie-->

        <div class="container">
            <h1><u>Facturer un service</u></h1>
            <p>Choisissez un client et le service à lui facturer.</p>
            <div class="row">
                <form role="form" class="col-xs-3">

                <div class="form-group">
                    <label>Identifiant du client</label>
                    <input type="text" id="id_client" name="id_client" class="form-control">
                </div>

                <div class="form-group">
                    <label>Service à facturer</label>
                    <select class="form-control" id="select_service">
                    </select>
                </div>

                <input type="button" class="btn btn-default" onclick="insertData()" value="Valider"/>
                </form>
            </div>
        </div>

        <!--affichage du résultat de la requête-->
<!-- 
        <div class="container">
            <h3 id="resultTitle">Résultat de la demande</h3>
            <table class="table" id="table">
            <thead>
              <tr>
                <th>Numéro de client</th>
                <th>Nom</th>
                <th>Prénom</th>
                <th>Identifiant de la réservation</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
            </table>
        </div> -->
        
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

        <script src="script.js"></script>
    </body>
</html>
