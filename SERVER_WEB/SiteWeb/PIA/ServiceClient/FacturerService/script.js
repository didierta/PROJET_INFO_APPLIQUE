// function insertData() {

// 	if (!inputIsValid()) {
// 		alert("erreur : veuillez remplir les champs correctement");
// 		return;
// 	}

// 	var id_hotel = document.getElementById("id_hotel").value;

// 	//affiche le nombre total de client dans le titre du formulaire (gros doublon pas opti mais fonctionnel)
//     var xhr = new XMLHttpRequest();
// 	xhr.onreadystatechange =   	
// 		function() {
// 			console.log("xhr.status :" + xhr.status);
// 	        if(xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0)) {
// 	        	//$("#table tr not(:first)").remove();
// 	        	$("#resultTitle").text(xhr.responseText);
// 	        }
// 	    };
//     xhr.open("GET", "functions.php?id_hotel=" + id_hotel + "&mode=title", true);
//     xhr.send(null);

// 	//insère les td dans la table
//     var xhr2 = new XMLHttpRequest();
// 	xhr2.onreadystatechange =   	
// 		function() {
// 			console.log("xhr.status :" + xhr2.status);
// 	        if(xhr2.readyState == 4 && (xhr2.status == 200 || xhr2.status == 0)) {
// 	        	//$("#table tr not(:first)").remove();
// 	        	$("table").find("tr:gt(0)").remove();
// 	        	$("#table").append(xhr2.responseText);
// 	        }
// 	    };
//     xhr2.open("GET", "functions.php?id_hotel=" + id_hotel + "&mode=rows", true);
//     xhr2.send(null);
// }

function getServices() {

    var xhr = new XMLHttpRequest();
	xhr.onreadystatechange =   	
		function() {
			console.log("xhr.status :" + xhr.status);
	        if(xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0)) {
	        	alert(xhr.responseText);
	        	$("#select_service").append(xhr.responseText);
	        	//document.getElementById("select_service").text = xhr.responseText;
	        }
	    };
    xhr.open("GET", "functions.php?request=services", true);
    xhr.send(null);
}


/********************************************************
			Garantit le bon remplissage des champs
********************************************************/
function verifId() {

	var input_hotel = document.getElementById("id_hotel");
	
	console.log("verifId :");
	if(isNaN(input_hotel.value)) {
	  surligne(input_hotel, true);
	  return false;
	} else {
	  surligne(input_hotel, false);
	  return true;
	}
}

function surligne(champ, erreur) {
   if(erreur) {
      champ.style.backgroundColor = "#fba";
   } else {
      champ.style.backgroundColor = "";
   }
}

function inputIsValid() {

	var input_hotel = document.getElementById("id_hotel");

	if (input_hotel.style.backgroundColor == "") {
		return true;
	}
	return false;
}

getServices();