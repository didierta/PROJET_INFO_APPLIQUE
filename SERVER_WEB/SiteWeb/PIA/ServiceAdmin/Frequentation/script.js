function getData() {

	if (!inputIsValid()) {
		alert("erreur : veuillez remplir les champs correctement");
		return;
	}

	var id_hotel = document.getElementById("id_hotel").value;
	var date_deb = document.getElementById("date_deb").value;
	var date_fin = document.getElementById("date_fin").value;

    var xhr = new XMLHttpRequest();
	xhr.onreadystatechange =   	
		function() {
			console.log("xhr.status :" + xhr.status);
	        if(xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0)) {
	        	//$("#table tr not(:first)").remove();
	        	$("table").find("tr:gt(0)").remove();
	        	$("#table").append(xhr.responseText);
	        }
	    };
    xhr.open("GET", "functions.php?id_hotel=" + id_hotel + "&date_deb=" + date_deb + "&date_fin=" + date_fin, true);
    xhr.send(null);
}


/********************************************************
			Garantit le bon remplissage des champs
********************************************************/
function verifId() {

	var input_hotel = document.getElementById("id_hotel");
	
	console.log("verifId :");
	if(isNaN(input_hotel.value)) {
	  surligne(input_hotel, true);
	  return false;
	} else {
	  surligne(input_hotel, false);
	  return true;
	}
}

function verifDate() {

	var input_dateDeb = document.getElementById("date_deb");
	var input_dateFin = document.getElementById("date_fin");

	console.log("verifDate :");
	if(input_dateDeb.value >= input_dateFin.value) {
	  surligne(input_dateDeb, true);
	  surligne(input_dateFin, true);
	  return false;
	} else {
	  surligne(input_dateDeb, false);
	  surligne(input_dateFin, false);
	  return true;
	}
}

function surligne(champ, erreur) {
   if(erreur) {
      champ.style.backgroundColor = "#fba";
   } else {
      champ.style.backgroundColor = "";
   }
}

function inputIsValid() {

	var input_hotel = document.getElementById("id_hotel");
	var input_dateDeb = document.getElementById("date_deb");
	var input_dateFin = document.getElementById("date_fin");

	if (input_hotel.style.backgroundColor == "" &&
		input_dateDeb.style.backgroundColor == "" &&
		input_dateFin.style.backgroundColor == "") {
		return true;
	}
	return false;
}