<?php

	require("../../db.php");

    function initTabTypes($idHotel, $nbJoursTotal) {

        $connection = createConnection();

        //récupère les types
        $req = "SELECT LibelleType
                FROM TYPE";
        $queryTypes = $connection->query($req);

        //pour chaque type
        while($rowType = $queryTypes->fetch_assoc()) {

            $arrayTypes[$rowType["LibelleType"]]["dispo"] = 0;
            $arrayTypes[$rowType["LibelleType"]]["facture"] = 0;

            //récupère les chambres de l'hotel de ce type
            $req = "SELECT  *
                    FROM    CHAMBRE
                    WHERE   IdHotel = ".$idHotel." AND LibelleType = '".$rowType["LibelleType"]."'";
            $queryChambres = $connection->query($req);

            //pour chaque chambre
            while($rowChambres = $queryChambres->fetch_assoc()) {

                $arrayTypes[$rowType["LibelleType"]]["dispo"] += $nbJoursTotal;
            }
        }
        return $arrayTypes;
    }

    //calcule l'écart - en jours - entre deux dates
    function getNbJours($dateDeb, $dateFin) {
        $datetimedeb = strtotime($dateDeb);
        $datetimefin = strtotime($dateFin);
        $secs = $datetimefin - $datetimedeb;
        return round($secs / 86400);
    }

    function getOccupancy($idHotel, $dateDeb, $dateFin) {

        $connection = createConnection();
        //mysqli_set_charset($connection, "utf8");

        $nbJoursTotal = getNbJours($dateDeb, $dateFin);

        $tabTypes = initTabTypes($idHotel, $nbJoursTotal);

        //récupère toutes les réservations qui concernent l'hotel donné sur la période donnée
        $req = "SELECT  IdReservation, DateDebSejour, DateFinSejour
                FROM    RESERVATION
                WHERE   IdReservation IN (
                            SELECT  IdReservation
                            FROM    AFFECTE
                            WHERE   IdHotel = ".$idHotel."
                        ) AND DateDebSejour >= '".$dateDeb."' AND DateDebSejour <= '".$dateFin."'
                        AND EtatReservation <> 'annule'";

        $queryReservations = $connection->query($req);

        if ($queryReservations->num_rows > 0) {

            //pour chaque réservation :
            while($rowRes = $queryReservations->fetch_assoc()) {

                //si la borne supérieure donnée par l'utilisateur
                //est inférieure à la date de fin de la réservation,
                //on utilise celle-ci pour le calcul du nombre de jours.
                if ($rowRes["DateFinSejour"] > $dateFin) {
                    $date = $dateFin;
                } else {
                    $date = $rowRes["DateFinSejour"];
                }

                $nbJoursRes = getNbJours($rowRes["DateDebSejour"], $date);
                //echo "\$nbJoursRes=".$nbJoursRes."<br>";

                //récupère toutes les chambres qui sont associées à cette réservation
                $req = "SELECT  IdChambre, LibelleType
                        FROM    CHAMBRE
                        WHERE   IdChambre IN (
                                    SELECT  IdChambre
                                    FROM    AFFECTE
                                    WHERE   IdReservation = ".$rowRes["IdReservation"]."
                                ) AND IdHotel = ".$idHotel;

                $queryChambres = $connection->query($req);

                if ($queryChambres->num_rows > 0) {
                    
                    //pour chaque chambre :
                    while($rowChambre = $queryChambres->fetch_assoc()) {

                        // ajuste le nombre de jours disponible et facturé pour le type considéré
                        $tabTypes[$rowChambre["LibelleType"]]["dispo"] -= $nbJoursRes;
                        $tabTypes[$rowChambre["LibelleType"]]["facture"] += $nbJoursRes;
                    }
                }
            }
        }

        $result = array("nbJours" => $nbJoursTotal,
                        "tabTypes"=> $tabTypes);

        return $result;
    }

    /********************************************************
				  Génère les lignes du tableau
	********************************************************/

	$response = getOccupancy($_GET["id_hotel"],
							 $_GET["date_deb"],
							 $_GET["date_fin"]);

	if ($response["nbJours"] != 0) {

        foreach($response["tabTypes"] as $type => $key) {

            //n'affiche pas les types de chambre qui n'existent pas dans cet hotel
            if (intval($key["dispo"]) == 0 && intval($key["facture"]) == 0) continue;

            echo "<tr>";
            echo "<td>".$type."</td>";

            echo "<td>".$key["dispo"]."</td>";
            echo "<td>".$key["facture"]."</td>";

            $occupation = intval($key["facture"]);
            $total = intval($key["dispo"]) + intval($key["facture"]);

            $ratio = round (100 * ($occupation / $total));
            echo "<td>".$ratio."%</td>";
            echo "</tr>";
        }
	}

?>