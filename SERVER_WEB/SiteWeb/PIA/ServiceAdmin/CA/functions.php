<?php

	require("../../db.php");

    function getPercentage($numerator, $divider) {
        return round (100 * (intval($numerator) / intval($divider)));
    }

    function initTabServices() {

        $connection = createConnection();
                
        $req = "SELECT LibelleService
                FROM SERVICE";
        $listeServices = $connection->query($req);

        if ($listeServices->num_rows > 0) {
            while($rowService = $listeServices->fetch_assoc()) {
                $arrayServices[$rowService["LibelleService"]] = 0;
            }
        } else {
            echo "Aucun service enregistré <br>";
        }
        return $arrayServices;
    }

    function getProfit($IdHotel, $DateDeb, $DateFin) {

        $connection = createConnection();
        //mysqli_set_charset($connection, "utf8");

        $total = 0;
        $totalChambre = 0;
        $tabServices = initTabServices();

        //récupère toutes les réservations qui concernent l'hotel donné sur la période donnée
        $req = "SELECT  IdReservation, MontantTotal
                FROM    RESERVATION
                WHERE   IdReservation IN (
                            SELECT  IdReservation
                            FROM    AFFECTE
                            WHERE   IdHotel = ".$IdHotel."
                        ) AND DateDebSejour >= \"".$DateDeb."\" AND DateFinSejour <= \"".$DateFin."\"
                        AND EtatReservation <> \"annule\"";

        $queryReservations = $connection->query($req);

        if ($queryReservations->num_rows > 0) {

            //pour chaque réservation :
            while($rowRes = $queryReservations->fetch_assoc()) {

                //incrémente le montant total pour l'hotel considéré
                $total += intval($rowRes["MontantTotal"]);

                //récupère tous les services qui sont facturés à cette réservation
                $req = "SELECT  LibelleService, NbFois
                        FROM    CONTIENT
                        WHERE   IdReservation = ".$rowRes["IdReservation"];
                $queryServices = $connection->query($req);

                if ($queryServices->num_rows > 0) {
                    
                    //pour chaque service :
                    while($rowService = $queryServices->fetch_assoc()) {

                        //récupère son montant
                        $req = "SELECT  MontantService
                                FROM    SERVICE
                                WHERE   LibelleService = \"".$rowService["LibelleService"]."\"";

                        $queryMontant = $connection->query($req);

                        $rowMontant = $queryMontant->fetch_assoc();

                        // incrémente le montant total facturé à ce service pour l'hotel considéré 
                        $tabServices[$rowService["LibelleService"]] += intval($rowMontant["MontantService"]) * intval($rowService["NbFois"]);
                    }
                }
            }
        }

        //calcul du montant facturé aux chambres
        $totalChambre = $total;
        foreach($tabServices as $key => $value) {
            $totalChambre -= $value;
        }

        $result = array("total" => $total,
                        "totalChambre" => $totalChambre,
                        "tabServices"=> $tabServices);

        return $result;
    }

    /********************************************************
				  Génère les lignes du tableau
	********************************************************/

	$response = getProfit($_GET["id_hotel"],
						  $_GET["date_deb"],
						  $_GET["date_fin"]);

    if ($_GET["mode"] == "title") {

        echo "Chiffre d'affaire = ".$response["total"];

    } else if ($_GET["mode"] == "rows") { 

    	if ($response["totalChambre"] != 0) {
    		//2eme ligne : Chambres
    		echo "<tr>";
    		echo "<td>Chambres</td>";
    		echo "<td>".$response["totalChambre"]."</td>";
            echo "<td>".getPercentage($response["totalChambre"], $response["total"])."%</td>";
    		echo "</tr>";
    		//3eme à n-ieme ligne : Services
    		foreach($response["tabServices"] as $key => $value) {
    			echo "<tr>";
    			echo "<td>".$key."</td>";
    			echo "<td>".$value."</td>";
                echo "<td>".getPercentage($value, $response["total"])."%</td>";
    			echo "</tr>";
    		}
    	}
    }

?>