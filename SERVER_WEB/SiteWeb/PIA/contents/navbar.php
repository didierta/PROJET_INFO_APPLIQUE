<div class="navbar navbar-inverse navbar-fixed-top">
	<div class="container">
		<a class="navbar-brand" href="/PIA/index.php">Accueil</a>
		<ul class="nav navbar-nav">
			<li><a href="#">Service réservation</a></li>
			<!--<li><a href="#">Service clientèle</a></li>-->
			<li class="dropdown">
	          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Service clientèle <span class="caret"></span></a>
	          <ul class="dropdown-menu" role="menu">
	            <li><a href="/PIA/ServiceClient/ListeClients/index.php">Clientèle actuelle</a></li>
	            <li class="divider"></li>
	            <li><a href="/PIA/ServiceClient/Historique/index.php">Historique clientèle</a></li>
	            <li class="divider"></li>
	            <li><a href="/PIA/ServiceClient/FacturerService/index.php">Facturer un service</a></li>
	          </ul>
	        </li>
			<li><a href="#">Service facturation</a></li>
			<li class="dropdown">
	          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Service administration <span class="caret"></span></a>
	          <ul class="dropdown-menu" role="menu">
	            <li><a href="/PIA/ServiceAdmin/CA/index.php">Chiffre d'affaire</a></li>
	            <li class="divider"></li>
	            <li><a href="/PIA/ServiceAdmin/Frequentation/index.php">Fréquentation</a></li>
	          </ul>
	        </li>
		</ul>
	</div>
</div>
<br/><br/><br/><br/>