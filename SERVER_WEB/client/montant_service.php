<!DOCTYPE html>
<!-- http://jeremy.philippe.etu.perso.luminy.univ-amu.fr/PIA/service_client.php -->
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<html>
<body> <span style="font-weight:bold;">Montant Service Client</span><br>

<form enctype="multipart/form-data" method="post" action="montant_service.php">
    <fieldset>
        <legend>Rentrer l'identifiant du Client</legend><br>
            Id du Client : <input type="text" name="id_client" id="id_client"><br>
            
        <input type="submit" value="Envoyer" name="valider"/><br>
    </fieldset>
</form>

<table border=1>

<?php
    require("../DB/db.php");

// connexion à la base
    $connection = createConnection();

// on crée la requête SQL
    $sql = "SELECT CLIENT.IdClient, CLIENT.Nom, CLIENT.Prenom, SUM(SERVICE.MontantService) as montant_total  FROM CLIENT
        INNER JOIN RESERVATION ON RESERVATION.IdClient      = CLIENT.IdClient
        INNER JOIN CONTIENT ON RESERVATION.IdReservation    = CONTIENT.IdReservation
        INNER JOIN SERVICE ON CONTIENT.LibelleService       = SERVICE.LibelleService GROUP BY CLIENT.IdClient";

    $result = mysql_query($sql) or die('Erreur SQL !<br>'.$sql.'<br>'.mysql_error());

    echo '<tr>';
    echo '<td> IdClient </td>';
    echo '<td> Nom      </td>';
    echo '<td> Prenom   </td>'; 
    echo '<td> MontantService </td>'; 
    echo '</tr>';

    while($data = $result->fetch_assoc())
    {
    // on affiche les informations de l'enregistrement en cours
    echo '<tr>';
    echo '<td>'.$data['IdClient'].'</td>';
    echo '<td>'.$data['Nom'].'</td>';
    echo '<td>'.$data['Prenom'].'</td>'; 
    echo '<td>'.$data['montant_total'].'</td>'; 
    echo '</tr>';
    }
    echo '</table>';

    // Free result set
    mysqli_free_result($result);

    $connection->close();
?>

<form>
    <input type="button" value="Retour" onclick="history.go(-1)">
</form>
</body>
</html>