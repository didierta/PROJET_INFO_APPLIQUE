INSERT INTO CLIENT 
(IdClient, Nom, Prenom, Adresse, Telephone, Courriel, LibelleCategorie)
VALUES (1, 'THEBAUD', 'Antoine', 'XXXXX', NULL, NULL, 'ISOLÉ').

INSERT INTO RESERVATION
(IdReservation, DateReservation, DateDebSejour, DateFinSejour, EtatReservation, IdFacture, DatePaiement, MontantTotal, ArrhesReglees, IdClient)
VALUES (1, '2015-03-04', '2015-03-30', '2015-04-04', 'CONFIRMÉE', 1, NULL, NULL, 1, 1);

INSERT INTO RESERVATION
(IdReservation, DateReservation, DateDebSejour, DateFinSejour, EtatReservation, IdFacture, DatePaiement, MontantTotal, ArrhesReglees, IdClient)
VALUES (2, '2015-03-04', '2015-03-20', '2015-04-30', 'CONFIRMÉE', 1, NULL, NULL, 1, 1);
