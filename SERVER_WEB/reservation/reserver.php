<?php

	require("../DB/db.php");

	function idHotelFilledCorrectly() {
		return isset( $_POST["id_hotel"] ) &&
			   ! empty( $_POST["id_hotel"] );
	}

	function nbRoomsFilledCorrectly() {
		return isset( $_POST["nb_rooms"] ) &&
			   ! empty( $_POST["nb_rooms"] );
	}

	function nbPersonsFilledCorrectly() {
		return isset( $_POST["nb_people"] ) &&
			   ! empty( $_POST["nb_people"] );
	}
	

	function dateDebFilledCorrectly() {
		return isset( $_POST["date_deb"] ) &&
			   ! empty( $_POST["date_deb"] );
	}

	function dateFinFilledCorrectly() {
		return isset( $_POST["date_fin"] ) &&
			   ! empty( $_POST["date_fin"] );
	}

	function comfortFilled() {
		return isset($_POST["confort"]) &&
				! empty($_POST["confort"]);
	}

	function formIsWellFilled() {
		return idHotelFilledCorrectly() &&
			   nbRoomsFilledCorrectly() &&
			   nbPersonsFilledCorrectly() &&
			   dateDebFilledCorrectly() &&
			   dateFinFilledCorrectly() &&
			   comfortFilled();
	}
	


	function reserve() {
		if (! formIsWellFilled()) {
			header("Location : reservation.php");
			echo "<script> alert('Veuillez bien remplir tous les champs du formulaire!') </script>";
			return;
		}


		$id_hotel = $_POST["id_hotel"];
		$nb_rooms = $_POST["nb_rooms"];
		$nb_people = $_POST["nb_people"];
		$date_deb = $_POST["date_deb"];
		$date_fin = $_POST["date_fin"];
		$confort = $_POST["confort"];

		// echo $confort . "<br>";
		
		//TODO : enregistrer réservation dans la BD
		$connection = createConnection();
		
		$get_nb_rooms_available = " SELECT COUNT(*) as NB_ROOMS
									FROM AFFECTE
									INNER JOIN RESERVATION ON AFFECTE.IdReservation = RESERVATION.IdReservation
									INNER JOIN CHAMBRE ON AFFECTE.IdHotel = CHAMBRE.IdHotel AND AFFECTE.IdChambre = CHAMBRE.IdChambre
									INNER JOIN TYPE ON CHAMBRE.LibelleType = TYPE.LibelleType
									WHERE DateDebSejour NOT BETWEEN '$date_deb' AND '$date_fin'
										AND DateFinSejour NOT BETWEEN '$date_deb' AND '$date_fin'
										AND AFFECTE.IdHotel = $id_hotel
										AND CHAMBRE.LibelleType = '$confort'
								  ";


		$result = $connection->query($get_nb_rooms_available);

		if(! $result) {
			// echo ("Error : " . mysqli_error($connection) ."<br>");
			echo " Erreur : [ reserver() ]:  " . $connection->error . "<br>";
			return;
		}

		$row = $result->fetch_assoc();
		$nb_rooms_available = $row["NB_ROOMS"];


	/* not enough rooms available */
		if($nb_rooms > $nb_rooms_available) {
			echo "Il n'y a pas assez de chambre disponibles car : <br>";
			echo "Il y a seulement " . $row["NB_ROOMS"] . " chambres disponibles du ".$date_deb." au ".$date_fin."<br>";
			echo '<a href="reservation.php"> Retour à la page de réservation</a>';
			return;
		}

	/* several rooms are available */
		echo "La réservation peut être effectuée! :) <br>";
		echo "Il y $nb_rooms_available chambres disponibles <br>";


		$get_all_available_rooms = "SELECT AFFECTE.IdHotel, AFFECTE.IdChambre
									FROM AFFECTE
									INNER JOIN RESERVATION ON AFFECTE.IdReservation = RESERVATION.IdReservation
									INNER JOIN CHAMBRE ON AFFECTE.IdHotel = CHAMBRE.IdHotel AND AFFECTE.IdChambre = CHAMBRE.IdChambre
									INNER JOIN TYPE ON CHAMBRE.LibelleType = TYPE.LibelleType
									WHERE DateDebSejour NOT BETWEEN '$date_deb' AND '$date_fin'
										AND DateFinSejour NOT BETWEEN '$date_deb' AND '$date_fin'
										AND AFFECTE.IdHotel = $id_hotel
										AND CHAMBRE.LibelleType = '$confort'
								   ";
		$result = $connection->query($get_all_available_rooms);

		if(! $result) {
			// echo ("Error : " . mysqli_error($connection) ."<br>");
			echo " Erreur : [ reserver() ]:  " . $connection->error . "<br>";
			return;
		}

		if($result->num_rows > 0) {
			echo "<table border=\"1\">";
			echo "<tr>
					<th>IdHotel</th>
					<th>IdChambre</th>
				  </tr>";
			while( $reservation = $result->fetch_assoc() ) {
				echo "<tr>";
				echo "<td>" . $reservation["IdHotel"] ."</td>";
				echo "<td>" . $reservation["IdChambre"] ."</td>";

				echo "</tr>";
			}
			echo "</table>";
		}
		
	} // reserve()

	
 


?>


<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Reservation de chambres</title>
    </head>

    <body>
        <div>
        <?php
        	reserve();

        ?>

        </div>



        <!--
        <script type="text/javascript" src="./bootstrap/css/bootstrap.css"></script>

        --> 
        <script type="text/javascript" src="./js/reservation.js"></script>
    </body>
</html>
