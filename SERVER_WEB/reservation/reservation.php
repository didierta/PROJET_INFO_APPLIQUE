<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Reservation de chambres</title>
    </head>

    <body>
        <div>
            <form name="reserver" action="reserver.php" onsubmit="return check_fields();" method="post">

                Identifiant de l'hôtel : 
                    <input type="text" name="id_hotel" value="<?php echo $_POST["id_hotel"]?>"> <br>
                    
                Nombre de chambres : 
                    <input type="text" name="nb_rooms" value="<?php echo $_POST["nb_rooms"]?>"> <br>
                    
                Nombre de personnes : 
                    <input type="text" name="nb_people" value="<?php echo $_POST["nb_people"]?>"> <br>
                    
                Du : 
                    <input type="date" name="date_deb" value="<?php echo $_POST["date_deb"]?>"> <br>
                    
                au : 
                    <input type="date" name="date_fin" value="<?php echo $_POST["date_fin"]?>"> <br>
                Confort : 
                <select name="confort">
                    <option selected="selected">----</option>
                    <option value="LUXE">Luxe</option>
                    <option value="CONFORT">Confort</option>
                    <option value="STANDARD"> Standard</option>
                    <option value="TOURISME">Tourisme</option>
                </select>
                <br>
                <input type="submit" value="Réserver" title="reserve">

            </form>

        </div>



        <!--
        <script type="text/javascript" src="./bootstrap/css/bootstrap.css"></script>

        --> 
        <script type="text/javascript" src="../js/reservation.js"></script>
    </body>
</html>