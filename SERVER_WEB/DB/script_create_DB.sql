#------------------------------------------------------------
#        Script MySQL.
#------------------------------------------------------------


CREATE TABLE RESERVATION(
        IdReservation   Integer NOT NULL ,
        DateReservation Date NOT NULL ,
        DateDebSejour   Date NOT NULL ,
        DateFinSejour   Date NOT NULL ,
        EtatReservation Varchar (25) ,
        IdFacture       Integer NOT NULL ,
        MontantTotal    Int ,
        DatePaiement    Date ,
        ArrhesReglees   Bool NOT NULL ,
        IdClient        Integer NOT NULL ,
        PRIMARY KEY (IdReservation )
)ENGINE=InnoDB;


CREATE TABLE CLIENT(
        IdClient         Integer NOT NULL ,
        Nom              Varchar (25) NOT NULL ,
        Prenom           Varchar (25) NOT NULL ,
        Adr              Varchar (25) NOT NULL ,
        Tel              Varchar (25) ,
        Courriel         Varchar (25) ,
        LibelleCategorie Varchar (25) ,
        PRIMARY KEY (IdClient )
)ENGINE=InnoDB;


CREATE TABLE CHAMBRE(
        IdChambre     Integer NOT NULL ,
        IdHotel       Integer NOT NULL ,
        LibelleType   Varchar (25) ,
        PRIMARY KEY (IdChambre ,IdHotel )
)ENGINE=InnoDB;


CREATE TABLE HOTEL(
        IdHotel      Integer NOT NULL ,
        NomHotel     Varchar (25) ,
        AdresseHotel Varchar (25) ,
        TelHotel     Varchar (25) ,
        PRIMARY KEY (IdHotel )
)ENGINE=InnoDB;


CREATE TABLE TYPE(
        LibelleType   Varchar (25) NOT NULL ,
        NbLitsSimples Integer ,
        NbLitsDoubles Integer ,
        PossedeTel    Bool ,
        PossedeTV     Bool ,
        PossedeHD     Bool ,
        PrixType      Integer ,
        PRIMARY KEY (LibelleType )
)ENGINE=InnoDB;


CREATE TABLE CATEGORIE(
        LibelleCategorie Varchar (25) NOT NULL ,
        Pourcentage      Integer ,
        PRIMARY KEY (LibelleCategorie )
)ENGINE=InnoDB;


CREATE TABLE SERVICE(
        LibelleService Varchar (25) NOT NULL ,
        MontantService Int NOT NULL ,
        PRIMARY KEY (LibelleService )
)ENGINE=InnoDB;


CREATE TABLE AFFECTE(
        IdChambre     Integer NOT NULL ,
        IdHotel       Integer NOT NULL ,
        IdReservation Integer NOT NULL ,
        PRIMARY KEY (IdChambre ,IdHotel ,IdReservation )
)ENGINE=InnoDB;


CREATE TABLE ACCOMPAGNE(
        IdClient      Integer NOT NULL ,
        IdReservation Integer NOT NULL ,
        PRIMARY KEY (IdClient ,IdReservation )
)ENGINE=InnoDB;


CREATE TABLE CONTIENT(
        IdReservation  Integer NOT NULL ,
        LibelleService Varchar (25) NOT NULL ,
        NbFois         Int NOT NULL ,
        PRIMARY KEY (IdReservation ,LibelleService )
)ENGINE=InnoDB;

ALTER TABLE RESERVATION ADD CONSTRAINT FK_RESERVATION_IdClient FOREIGN KEY (IdClient) REFERENCES CLIENT(IdClient);
ALTER TABLE CLIENT ADD CONSTRAINT FK_CLIENT_LibelleCategorie FOREIGN KEY (LibelleCategorie) REFERENCES CATEGORIE(LibelleCategorie);
ALTER TABLE CHAMBRE ADD CONSTRAINT FK_CHAMBRE_IdHotel_HOTEL FOREIGN KEY (IdHotel) REFERENCES HOTEL(IdHotel);
ALTER TABLE CHAMBRE ADD CONSTRAINT FK_CHAMBRE_LibelleType FOREIGN KEY (LibelleType) REFERENCES TYPE(LibelleType);
ALTER TABLE AFFECTE ADD CONSTRAINT FK_AFFECTE_IdChambre FOREIGN KEY (IdChambre) REFERENCES CHAMBRE(IdChambre);
ALTER TABLE AFFECTE ADD CONSTRAINT FK_AFFECTE_IdHotel FOREIGN KEY (IdHotel) REFERENCES HOTEL(IdHotel);
ALTER TABLE AFFECTE ADD CONSTRAINT FK_AFFECTE_IdReservation FOREIGN KEY (IdReservation) REFERENCES RESERVATION(IdReservation);
ALTER TABLE ACCOMPAGNE ADD CONSTRAINT FK_ACCOMPAGNE_IdClient FOREIGN KEY (IdClient) REFERENCES CLIENT(IdClient);
ALTER TABLE ACCOMPAGNE ADD CONSTRAINT FK_ACCOMPAGNE_IdReservation FOREIGN KEY (IdReservation) REFERENCES RESERVATION(IdReservation);
ALTER TABLE CONTIENT ADD CONSTRAINT FK_CONTIENT_IdReservation FOREIGN KEY (IdReservation) REFERENCES RESERVATION(IdReservation);
ALTER TABLE CONTIENT ADD CONSTRAINT FK_CONTIENT_LibelleService FOREIGN KEY (LibelleService) REFERENCES SERVICE(LibelleService);
