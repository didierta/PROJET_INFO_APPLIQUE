<?php
	class Hotel {
		private $hotelId;
		private $hotelName;
		private $hotelAddress;
		private $hotelPhoneNumber;

        function __construct($hotelId, $hotelName, $hotelAddress, $hotelPhoneNumber)
        {
            $this->hotelId = $hotelId;
            $this->hotelName = $hotelName;
            $this->hotelAddress = $hotelAddress;
            $this->hotelPhoneNumber = $hotelPhoneNumber;
        }

        /**
         * @return mixed
         */
        public function getHotelId()
        {
            return $this->hotelId;
        }

        /**
         * @param mixed $hotelId
         */
        public function setHotelId($hotelId)
        {
            $this->hotelId = $hotelId;
        }

        /**
         * @return mixed
         */
        public function getHotelName()
        {
            return $this->hotelName;
        }

        /**
         * @param mixed $hotelName
         */
        public function setHotelName($hotelName)
        {
            $this->hotelName = $hotelName;
        }

        /**
         * @return mixed
         */
        public function getHotelAddress()
        {
            return $this->hotelAddress;
        }

        /**
         * @param mixed $hotelAddress
         */
        public function setHotelAddress($hotelAddress)
        {
            $this->hotelAddress = $hotelAddress;
        }

        /**
         * @return mixed
         */
        public function getHotelPhoneNumber()
        {
            return $this->hotelPhoneNumber;
        }

        /**
         * @param mixed $hotelPhoneNumber
         */
        public function setHotelPhoneNumber($hotelPhoneNumber)
        {
            $this->hotelPhoneNumber = $hotelPhoneNumber;
        }




    }


?>