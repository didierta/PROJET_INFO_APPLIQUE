<?php
/**
 * Created by PhpStorm.
 * User: Didier_TA
 * Date: 26/03/2015
 * Time: 10:47
 */

    class Service {
        private $serviceWording;
        private $serviceAmount;

        function __construct($serviceWording, $serviceAmount)
        {
            $this->serviceWording = $serviceWording;
            $this->serviceAmount = $serviceAmount;
        }


        public function getServiceWording()
        {
            return $this->serviceWording;
        }


        public function setServiceWording($serviceWording)
        {
            $this->serviceWording = $serviceWording;
        }


        public function getServiceAmount()
        {
            return $this->serviceAmount;
        }


        public function setServiceAmount($serviceAmount)
        {
            $this->serviceAmount = $serviceAmount;
        }




    }


?>