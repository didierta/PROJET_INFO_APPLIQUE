<?php
	class Customer {
		private $customerId;
		private $customerLastName;
		private $customerFirstName;
		private $customerAddress;
		private $customerPhoneNumber;
		private $customerMail;
		private $categoryWording;

        function __construct($customerId, $customerLastName, $customerFirstName, $customerAddress, $customerPhoneNumber, $customerMail, $categoryWording)
        {
            $this->customerId = $customerId;
            $this->customerLastName = $customerLastName;
            $this->customerFirstName = $customerFirstName;
            $this->customerAddress = $customerAddress;
            $this->customerPhoneNumber = $customerPhoneNumber;
            $this->customerMail = $customerMail;
            $this->categoryWording = $categoryWording;
        }

        /**
         * @return mixed
         */
        public function getCustomerId()
        {
            return $this->customerId;
        }

        /**
         * @param mixed $customerId
         */
        public function setCustomerId($customerId)
        {
            $this->customerId = $customerId;
        }

        /**
         * @return mixed
         */
        public function getCustomerLastName()
        {
            return $this->customerLastName;
        }

        /**
         * @param mixed $customerLastName
         */
        public function setCustomerLastName($customerLastName)
        {
            $this->customerLastName = $customerLastName;
        }

        /**
         * @return mixed
         */
        public function getCustomerFirstName()
        {
            return $this->customerFirstName;
        }

        /**
         * @param mixed $customerFirstName
         */
        public function setCustomerFirstName($customerFirstName)
        {
            $this->customerFirstName = $customerFirstName;
        }

        /**
         * @return mixed
         */
        public function getCustomerAddress()
        {
            return $this->customerAddress;
        }

        /**
         * @param mixed $customerAddress
         */
        public function setCustomerAddress($customerAddress)
        {
            $this->customerAddress = $customerAddress;
        }

        /**
         * @return mixed
         */
        public function getCustomerPhoneNumber()
        {
            return $this->customerPhoneNumber;
        }

        /**
         * @param mixed $customerPhoneNumber
         */
        public function setCustomerPhoneNumber($customerPhoneNumber)
        {
            $this->customerPhoneNumber = $customerPhoneNumber;
        }

        /**
         * @return mixed
         */
        public function getCustomerMail()
        {
            return $this->customerMail;
        }

        /**
         * @param mixed $customerMail
         */
        public function setCustomerMail($customerMail)
        {
            $this->customerMail = $customerMail;
        }

        /**
         * @return mixed
         */
        public function getCategoryWording()
        {
            return $this->categoryWording;
        }

        /**
         * @param mixed $categoryWording
         */
        public function setCategoryWording($categoryWording)
        {
            $this->categoryWording = $categoryWording;
        }




    }

?>