<?php
	class Type {
		private $typeWording;
		private $nbOfSingleBeds;
		private $nbOfDoubleBeds;
		private $hasPhone;
		private $hasTV;
		private $hasHD;
		private $typePrice;

        function __construct($typeWording, $nbOfSingleBeds, $nbOfDoubleBeds, $hasPhone, $hasTV, $hasHD, $typePrice)
        {
            $this->typeWording = $typeWording;
            $this->nbOfSingleBeds = $nbOfSingleBeds;
            $this->nbOfDoubleBeds = $nbOfDoubleBeds;
            $this->hasPhone = $hasPhone;
            $this->hasTV = $hasTV;
            $this->hasHD = $hasHD;
            $this->typePrice = $typePrice;
        }

        /**
         * @return mixed
         */
        public function getTypeWording()
        {
            return $this->typeWording;
        }

        /**
         * @param mixed $typeWording
         */
        public function setTypeWording($typeWording)
        {
            $this->typeWording = $typeWording;
        }

        /**
         * @return mixed
         */
        public function getNbOfSingleBeds()
        {
            return $this->nbOfSingleBeds;
        }

        /**
         * @param mixed $nbOfSingleBeds
         */
        public function setNbOfSingleBeds($nbOfSingleBeds)
        {
            $this->nbOfSingleBeds = $nbOfSingleBeds;
        }

        /**
         * @return mixed
         */
        public function getNbOfDoubleBeds()
        {
            return $this->nbOfDoubleBeds;
        }

        /**
         * @param mixed $nbOfDoubleBeds
         */
        public function setNbOfDoubleBeds($nbOfDoubleBeds)
        {
            $this->nbOfDoubleBeds = $nbOfDoubleBeds;
        }

        /**
         * @return mixed
         */
        public function getHasPhone()
        {
            return $this->hasPhone;
        }

        /**
         * @param mixed $hasPhone
         */
        public function setHasPhone($hasPhone)
        {
            $this->hasPhone = $hasPhone;
        }

        /**
         * @return mixed
         */
        public function getHasTV()
        {
            return $this->hasTV;
        }

        /**
         * @param mixed $hasTV
         */
        public function setHasTV($hasTV)
        {
            $this->hasTV = $hasTV;
        }

        /**
         * @return mixed
         */
        public function getHasHD()
        {
            return $this->hasHD;
        }

        /**
         * @param mixed $hasHD
         */
        public function setHasHD($hasHD)
        {
            $this->hasHD = $hasHD;
        }

        /**
         * @return mixed
         */
        public function getTypePrice()
        {
            return $this->typePrice;
        }

        /**
         * @param mixed $typePrice
         */
        public function setTypePrice($typePrice)
        {
            $this->typePrice = $typePrice;
        }




    }

?>