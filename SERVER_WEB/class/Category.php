<?php
	class Category {
		private $categoryWording;
		private $reductionPercentage;

        function __construct($categoryWording, $reductionPercentage)
        {
            $this->categoryWording = $categoryWording;
            $this->reductionPercentage = $reductionPercentage;
        }


        public function getCategoryWording()
        {
            return $this->categoryWording;
        }


        public function setCategoryWording($categoryWording)
        {
            $this->categoryWording = $categoryWording;
        }


        public function getReductionPercentage()
        {
            return $this->reductionPercentage;
        }


        public function setReductionPercentage($reductionPercentage)
        {
            $this->reductionPercentage = $reductionPercentage;
        }

	}

?>