<?php
/**
 * Created by PhpStorm.
 * User: Didier_TA
 * Date: 26/03/2015
 * Time: 10:32
 */


    class Reservation {
        private $idReservation;
        private $dateOfReservation;
        private $dateOfBeginningOfStay; // dateDebSej
        private $dateOfEndOfStay;
        private $reservationState;
        private $billId;
        private $dateOfPayment;
        private $totalAmount;
        private $depositePaid;
        private $customerId;

        function __construct($idReservation, $dateOfReservation, 
                             $dateOfBeginningOfStay, $dateOfEndOfStay, 
                             $billId, $dateOfPayment, 
                             $clientId)
        {
            $this->idReservation = $idReservation;
            $this->dateOfReservation = $dateOfReservation;
            $this->dateOfBeginningOfStay = $dateOfBeginningOfStay;
            $this->dateOfEndOfStay = $dateOfEndOfStay;
            $this->billId = $billId;
            $this->dateOfPayment = $dateOfPayment;
            $this->customerId = $clientId;

            $this->reservationState = 'EN ATTENTE DE CONFIRMATION';
            $this->totalAmount = 0;
            $this->depositePaid = 'FALSE';
        }


        public function getIdReservation()
        {
            return $this->idReservation;
        }


        public function setIdReservation($idReservation)
        {
            $this->idReservation = $idReservation;
        }


        public function getDateOfReservation()
        {
            return $this->dateOfReservation;
        }


        public function setDateOfReservation($dateOfReservation)
        {
            $this->dateOfReservation = $dateOfReservation;
        }


        public function getDateOfBeginningOfStay()
        {
            return $this->dateOfBeginningOfStay;
        }


        public function setDateOfBeginningOfStay($dateOfBeginningOfStay)
        {
            $this->dateOfBeginningOfStay = $dateOfBeginningOfStay;
        }


        public function getDateOfEndOfStay()
        {
            return $this->dateOfEndOfStay;
        }


        public function setDateOfEndOfStay($dateOfEndOfStay)
        {
            $this->dateOfEndOfStay = $dateOfEndOfStay;
        }


        public function getReservationState()
        {
            return $this->reservationState;
        }


        public function setReservationState($reservationState)
        {
            $this->reservationState = $reservationState;
        }


        public function getBillId()
        {
            return $this->billId;
        }


        public function setBillId($billId)
        {
            $this->billId = $billId;
        }


        public function getDateOfPayment()
        {
            return $this->dateOfPayment;
        }


        public function setDateOfPayment($dateOfPayment)
        {
            $this->dateOfPayment = $dateOfPayment;
        }


        public function getTotalAmount()
        {
            return $this->totalAmount;
        }


        public function setTotalAmount($totalAmount)
        {
            $this->totalAmount = $totalAmount;
        }


        public function getDepositePaid()
        {
            return $this->depositePaid;
        }


        public function setDepositePaid($depositePaid)
        {
            $this->depositePaid = $depositePaid;
        }


        public function getCustomerId()
        {
            return $this->customerId;
        }


        public function setCustomerId($customerId)
        {
            $this->customerId = $customerId;
        }

        


    }


?>